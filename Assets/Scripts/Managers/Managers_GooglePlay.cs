﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class Managers_GooglePlay : MonoBehaviour
{
    [SerializeField]
    Animator authAnimator;
    [SerializeField]
    Sounds sounds;

    bool authenticated;

	void Awake ()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
    }

    void Authenticate()
    {
        Social.localUser.Authenticate((bool success) => {
            this.authenticated = success;
        });
    }
	
    public void PostScore(int score)
    {
        if(this.authenticated)
        {
            Social.ReportScore(score, GPGSIds.leaderboard_lily_jump, (bool success) => {
                // handle success or failure
            });
        }
    }

    public void ShowLeaderboards()
    {
        if(this.authenticated)
            PlayGamesPlatform.Instance.ShowLeaderboardUI();
        else
        {
            this.sounds.Play(11, 1, 1, 0.9f);
            this.authAnimator.SetTrigger("Warning");
            Authenticate();
        }
    }
}
