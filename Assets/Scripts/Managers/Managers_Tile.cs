﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Managers_Tile : MonoBehaviour 
{
    // References
    [SerializeField]
    Replay replay;
    [SerializeField]
    ObjectReferences objectReferences;
    [SerializeField]
    List<GameObject> tilePrefabs;
    [SerializeField]
    GameObject winningTile, piranhaPrefab, flyPrefab, wingsPrefab, ripplePrefab, gradePrefab;
    [SerializeField]
    Transform tileSpawnPosition;
    [SerializeField]
    Tile_Global initialTile;
    [SerializeField]
    Text bonusText;
    [SerializeField]
    Image piranhaFill, piranhaBack;
    [SerializeField]
    Transform timeHudTransform, stageClearTransform;
    [SerializeField]
    Sounds sounds;
    Transform gradeObject;
    [SerializeField]
    Animator fadeOutAnimator, stageClearAnimator, bonusTextAnimator, timerAnimator, mainHudAnimator, titleScreenAnimator, tutorialAnimator;
    GameObject fly;
    Tile_Global currentTile;
    Animator wingsAnimator;

    // Stage Build
    [SerializeField]
    List<string> stageBuild;
    [SerializeField]
    List<float> stageTimesMultiplier;
    [SerializeField]
    List<string> bonusChances;

    // Jump Values
    [SerializeField]
    float moveAmountHop = 0, moveAmountLeap = 0;
    [SerializeField]
    int moveTimesHop = 0, moveTimesLeap = 0;

    // Tile Control
    [SerializeField]
    List<float> tileTimes;
    List<int> stageTilePool = new List<int>();
    List<GameObject> tilesList = new List<GameObject>();
    List<GameObject> sceneryComponents = new List<GameObject>();
    List<int> tileTypeHistory = new List<int>();
    List<GameObject> tileObjectHistory = new List<GameObject>();

    // Game Control
    bool gameStart = false, lastTile = false, powerUp = false, trapRemove = false, firstWave = false;
    float flyChanceBonus = 1, piranhaTimeBonus = 1;
    float stageTime, currentStageTime;

    int currentStage = 0;

    #region GETS & SETS

    public List<GameObject> SceneryComponents
    {
        get { return this.sceneryComponents; }
    }

    public Tile_Global CurrentTile
    {
        get { return this.currentTile; }
    }

    public bool PowerUp
    {
        get { return this.powerUp; }
        set { this.powerUp = true; }
    }

    public bool GameStart
    {
        set { this.gameStart = value; }
    }

    #endregion

    void Start () 
    {
        Screen.SetResolution(1080, 1920, true);
        if(this.replay.replaying)
        {
            StageBuild();
            StartCoroutine(MoveTilesDown(0, 4, this.moveTimesHop, this.moveAmountHop, false, false));
            this.mainHudAnimator.SetTrigger("On");
            this.titleScreenAnimator.SetTrigger("On");
        }
    }

    public void StartGame()
    {
        if(!this.firstWave)
        {
            this.firstWave = true;
            this.sounds.Play(6, 0.8f, 0.8f, 1);
            StageBuild();
            StartCoroutine(MoveTilesDown(0, 4, this.moveTimesHop, this.moveAmountHop, false, false));
            this.mainHudAnimator.SetTrigger("Appear");
            this.titleScreenAnimator.SetTrigger("Disappear");
        }
    }

    void StageBuild()
    {
        for (int i = 0; i < this.stageBuild[this.currentStage].Split(';').Length; i++)
        {
            for (int j = 0; j < int.Parse(this.stageBuild[this.currentStage].Split(';')[i]); j++)
            {
                int tileType;
                if (this.trapRemove && (i == 2 || i == 3))
                    tileType = 0;
                else
                    tileType = i;
                this.stageTilePool.Add(tileType);
                this.stageTime += this.tileTimes[tileType];
            }
        }
        this.stageTime *= this.stageTimesMultiplier[this.currentStage] * this.piranhaTimeBonus;
        if (!this.replay.tutorial)
            this.tutorialAnimator.SetTrigger("Enter");  
    }

    void Update()
    {
        if (this.gameStart && !this.objectReferences.Manager_GameOver.GameOver)
        {
            this.currentStageTime += Time.deltaTime;
            if (this.currentStageTime > this.stageTime)
            {
                this.objectReferences.Manager_GameOver.SetGameOver(true, 2);
                GameObject.Instantiate(this.piranhaPrefab, this.objectReferences.Obj_Player.transform.position, Quaternion.identity);
            }
            this.piranhaFill.fillAmount = this.currentStageTime / this.stageTime;
            float timerScale = 0.85f + 0.17f * Mathf.Clamp(this.currentStageTime / this.stageTime, 0, 1);
            this.timeHudTransform.localScale = new Vector2(timerScale, timerScale);
        } 
    }

    void OnMouseDown()
    {
        if (this.gameStart && !this.objectReferences.Manager_GameOver.GameOver)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (pos.x < 0)
            {
                this.objectReferences.Obj_Player.GetComponent<Animator>().SetTrigger("Jump");
                StartCoroutine(MoveTilesDown(0, 1, this.moveTimesHop, this.moveAmountHop, true, false));
            }
            else
            {
                this.objectReferences.Obj_Player.GetComponent<Animator>().SetTrigger("Jump");
                StartCoroutine(MoveTilesDown(0, 1, this.moveTimesLeap, this.moveAmountLeap, true, true));
            }
        }
    }

    IEnumerator MoveTilesDown(float time, int repeat, int moveTimes, float moveAmount, bool playerJump, bool doubleSpawn)
    {
        this.currentTile = null;
        for (int k = 0; k < repeat; k++)
        {
            this.gameStart = false;
            SpawnNextTile();
            for (int j = 0; j < moveTimes; j++)
            {
                yield return new WaitForSeconds(time);
                if(doubleSpawn && j == moveTimes / 2)
                    SpawnNextTile();
                for (int i = 0; i < this.tilesList.Count; i++)
                {
                    if(this.tilesList[i] != null)
                        this.tilesList[i].transform.Translate(new Vector3(0, -moveAmount, 0));
                }
                for(int i = 0; i < this.sceneryComponents.Count; i++)
                {
                    if (this.sceneryComponents[i] != null)
                        this.sceneryComponents[i].transform.Translate(new Vector3(0, -moveAmount, 0));
                }
            }
        }
        if (playerJump)
        {
            this.sounds.Play(0, 0.75f, 1.1f, 1);
            TileInteraction();
            if(!this.objectReferences.Manager_GameOver.GameOver)
            { 
                if (moveTimes == moveTimesHop)
                    this.objectReferences.Manager_Score.AddScore(1);
                if (moveTimes == moveTimesLeap)
                    this.objectReferences.Manager_Score.AddScore(2);
                this.objectReferences.Obj_Player.GetComponent<Animator>().SetTrigger("StopJump");
            }
        }
        if (!this.gameStart && this.tilesList.Count > 1)
        {
            if(!playerJump)
            {
                this.tilesList.Insert(0, this.initialTile.gameObject);
                this.gameStart = true;
            }
        }
    }

    void SpawnNextTile()
    {
        int tileChoosen;
        if (this.stageTilePool.Count > 0)
            tileChoosen = ChooseNextTile();
        else
        {
            if(lastTile)
                tileChoosen = 1;
            else
            {
                lastTile = true;
                tileChoosen = -1;
            }
        }
        
        this.tileTypeHistory.Add(tileChoosen);
        if(tileChoosen != -1)
            this.tilesList.Add(GameObject.Instantiate(this.tilePrefabs[tileChoosen], this.tileSpawnPosition.position, Quaternion.identity));
        
        if (tileChoosen == -1)
            this.tilesList.Add(GameObject.Instantiate(this.winningTile, this.tileSpawnPosition.position, Quaternion.identity));
        
        this.tileObjectHistory.Add(this.tilesList[this.tilesList.Count - 1]);
        if (this.tilesList.Count > 6)
        {
            Destroy(this.tilesList[0]);
            this.tilesList.RemoveAt(0);
        }
    }

    int ChooseNextTile()
    {
        int randomIndex = Random.Range(0, this.stageTilePool.Count);
        int tileGet = this.stageTilePool[randomIndex];
        if(this.tileTypeHistory.Count > 0)
            while(tileGet == 1 && tileTypeHistory[tileTypeHistory.Count - 1] == 1)
            {
                if (stageTilePool.Contains(0) || stageTilePool.Contains(2))
                {
                    randomIndex = Random.Range(0, this.stageTilePool.Count);
                    tileGet = this.stageTilePool[randomIndex];
                }
                else
                    tileGet = 0;
            }
        this.stageTilePool.RemoveAt(randomIndex);
        return tileGet;
    }

    void SpawnFly()
    {
        this.fly = GameObject.Instantiate(this.flyPrefab, new Vector3(5, 5, 0), Quaternion.identity);
    }

    public void SetPowerup()
    {
        this.sounds.Play(5, 1f, 1f, 1, true);
        if (!this.powerUp)
        {
            this.powerUp = true;
            Transform playerTransform = this.objectReferences.Obj_Player.transform;
            this.wingsAnimator = GameObject.Instantiate(this.wingsPrefab, playerTransform.position, Quaternion.identity, playerTransform).GetComponent<Animator>();
        }
    }

    public void RemovePowerUp()
    {
        this.sounds.Play(4, 1f, 1f, 0.45f, true);
        this.powerUp = false;
        this.wingsAnimator.SetTrigger("Destroy");
        this.objectReferences.Obj_Player.GetComponent<Animator>().SetTrigger("Jump");
        StartCoroutine(MoveTilesDown(0, 1, this.moveTimesHop, this.moveAmountHop, true, false));
    }

    void TileInteraction()
    {
        this.currentTile = this.tileObjectHistory[this.tileObjectHistory.Count - 5].GetComponent<Tile_Global>();
        this.currentTile.Interaction();
        int flyNumber = (int)Random.Range(0, 20 / this.flyChanceBonus);
        if (flyNumber == 0 && this.fly == null)
            SpawnFly();
    }

    public void CallRipple(Transform tile)
    {
        this.sounds.Play(1, 0.75f, 1.1f, 1);
        GameObject.Instantiate(this.ripplePrefab, tile.position, Quaternion.identity, tile);
    }

    public IEnumerator ShowGrade()
    {
        RemoveBonuses();
        this.tutorialAnimator.SetTrigger("Exit");
        this.stageClearAnimator.SetTrigger("Appear");
        this.gradeObject = GameObject.Instantiate(this.gradePrefab, this.stageClearTransform.position, Quaternion.identity).GetComponent<Transform>();
        int grade = 1;
        if (this.currentStageTime / this.stageTime < 0.65f)
            grade = 3;
        else if (this.currentStageTime / this.stageTime < 0.8f)
            grade = 2;
        this.sounds.Play(2, 0.5f, 0.5f, 0.83f);
        for (int i = 0; i < grade; i++)
        {
            yield return new WaitForSeconds(0.13f);
            gradeObject.GetChild(i).GetComponent<Animator>().SetTrigger("Start");
        }
        GetBonus(grade - 1);
        yield return new WaitForSeconds(0.8f);
        this.sounds.Play(3, 0.5f, 0.5f, 0.7f, true);
        this.fadeOutAnimator.SetTrigger("Next");
    }

    void GetBonus(int grade)
    {
        List<int> bonusPool = new List<int>();
        for (int i = 0; i < this.bonusChances[grade].Split(';').Length; i++)
        {
            for (int j = 0; j < int.Parse(bonusChances[grade].Split(';')[i]); j++)
            {
                bonusPool.Add(i);
            }
        }
        int bonusGot = bonusPool[Random.Range(0, bonusPool.Count)];
        switch(bonusGot)
        {
            case 0:
                this.bonusText.text = "No bonus";
            break;
            case 1:
                this.bonusText.text = "Bonus: More Flies";
                this.flyChanceBonus = 2;
            break;
            case 2:
                this.bonusText.text = "Bonus: Slow piranha";
                this.piranhaBack.color = Color.green;
                this.timerAnimator.ResetTrigger("Normal");
                this.timerAnimator.SetTrigger("Slow");
                this.piranhaTimeBonus = 1.4f;
            break;
            case 3:
                this.bonusText.text = "Bonus: No traps";
                this.trapRemove = true;
            break;
        }
        this.bonusTextAnimator.SetTrigger("Appear");
    }

    public void WinStage()
    {
        if (!this.replay.tutorial)
            this.replay.tutorial = true;
        for (int i = 0; i < this.sceneryComponents.Count; i++)
            GameObject.Destroy(this.sceneryComponents[i].gameObject);
        this.timeHudTransform.localScale = new Vector2(0.85f, 0.85f);
        this.piranhaFill.fillAmount = 0;
        if (fly != null)
            this.fly.GetComponent<Fly>().Destroy();
        this.bonusTextAnimator.SetTrigger("Disappear");
        this.stageClearAnimator.SetTrigger("Disappear");
        if (this.stageBuild.Count - 1 > this.currentStage)
            this.currentStage++;
        if (this.gradeObject != null)
            GameObject.Destroy(this.gradeObject.gameObject);
        this.tileObjectHistory = new List<GameObject>();
        this.tileTypeHistory = new List<int>();
        this.stageTilePool = new List<int>();
        for (int i = 0; i < this.tilesList.Count; i++)
            Destroy(tilesList[i]);
        this.tilesList = new List<GameObject>();
        this.gameStart = this.lastTile = false;
        this.stageTime = this.currentStageTime = 0;
        StageBuild();
        this.initialTile = GameObject.Instantiate(this.winningTile, this.objectReferences.Obj_Player.transform.position, Quaternion.identity).GetComponent<Tile_Global>();
        StartCoroutine(MoveTilesDown(0, 4, this.moveTimesHop, this.moveAmountHop, false, false));
    }

    void RemoveBonuses()
    {
        this.piranhaBack.color = Color.white;
        this.timerAnimator.SetTrigger("Normal");
        this.flyChanceBonus = this.piranhaTimeBonus = 1;
        this.trapRemove = false;
    }

    public void DestroyPlayer()
    {
        Destroy(this.objectReferences.Obj_Player);
    }
}
