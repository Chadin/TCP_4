﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Managers_GameOver : MonoBehaviour 
{
    [SerializeField]
    GameObject waterSplashPrefab;
    [SerializeField]
    Managers_GooglePlay manager_googlePlay;
    [SerializeField]
    Replay replay;
    [SerializeField]
    Sounds sounds;
    [SerializeField]
    ObjectReferences objReferences;
    [SerializeField]
    Managers_Tile tileManager;
    [SerializeField]
    Animator timerAnimator, gameOverAnimator, fadeOutAnimator, tutorialAnimator, player;
    [SerializeField]
    List<Sprite> gameOverImages;
    [SerializeField]
    Image gameOverImage;
    [SerializeField]
    Text scoreText, highScoreText;
    bool gameOver;

    public bool GameOver
    {
        get { return this.gameOver; }
    }
    
    public void ResetScene()
    {
        this.sounds.Play(6, 0.8f, 0.8f, 1);
        this.replay.replaying = true;
        this.fadeOutAnimator.SetTrigger("Start");
    }

    public void SetGameOver(bool timer, int image)
    {
        if(!timer && this.tileManager.PowerUp)
        {
            this.tileManager.RemovePowerUp();
        }
        else
        {
            this.tutorialAnimator.SetTrigger("Exit");
            this.gameOverImage.sprite = this.gameOverImages[image];
            this.timerAnimator.SetTrigger("Disappear");
            this.gameOver = true;
            Invoke("CallGameOverMenu", 1);
            if(image == 1)
                this.player.SetTrigger("Captured");
            if (image == 0)
            {
                GameObject.Instantiate(this.waterSplashPrefab, this.player.transform.position, Quaternion.identity);
                sounds.Play(10, 1, 1, 1);
            }
            
        }
    }

    void CallGameOverMenu()
    {
        this.gameOverAnimator.SetTrigger("Appear");
        int scoreGot = this.objReferences.Manager_Score.JumpScore;
        this.scoreText.text = scoreGot.ToString();
        if((PlayerPrefs.HasKey("HighScore") && scoreGot > PlayerPrefs.GetInt("HighScore")) || !PlayerPrefs.HasKey("HighScore"))
            PlayerPrefs.SetInt("HighScore", scoreGot);
        this.highScoreText.text = PlayerPrefs.GetInt("HighScore").ToString();
        this.manager_googlePlay.PostScore(scoreGot);
    }
}
