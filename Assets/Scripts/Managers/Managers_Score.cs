﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Managers_Score : MonoBehaviour 
{
    [SerializeField]
    Text scoreText;
    int jumpScore;

    public int JumpScore
    {
        get { return this.jumpScore; }
    }

    public void AddScore(int amount)
    {
        this.jumpScore += amount;
        this.scoreText.text = jumpScore.ToString();
    }
}
