﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class System_Persistent : MonoBehaviour
{
    [SerializeField]
    Replay replay;
    public static System_Persistent instance = null;

	void Awake ()
    {
        if (instance == null)
        { 
            instance = this;
            this.replay.replaying = false;
            this.replay.tutorial = false;
        }
        else if (instance != this)
            Destroy(this.gameObject);
        DontDestroyOnLoad(this.gameObject);
	}
	
}
