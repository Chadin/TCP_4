﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sound", menuName = "Sound")]
public class Sounds : ScriptableObject
{
    [SerializeField]
    GameObject audioPlayer;
    public List<AudioClip> sounds;

    public void Play(int sound, float minPitch, float maxPitch, float volume)
    {
        PlaySound(sound, minPitch, maxPitch, volume, 2f, false);
    }

    public void Play(int sound, float minPitch, float maxPitch, float volume, bool fade)
    {
        PlaySound(sound, minPitch, maxPitch, volume, 2f, true);
    }

    void PlaySound(int sound, float minPitch, float maxPitch, float volume, float time, bool fade)
    {
        GameObject player = GameObject.Instantiate(this.audioPlayer, Vector3.zero, Quaternion.identity);
        player.GetComponent<AudioSource>().pitch = Random.Range(minPitch, maxPitch);
        player.GetComponent<AudioSource>().clip = this.sounds[sound];
        player.GetComponent<AudioSource>().Play();
        player.GetComponent<AudioSource>().volume = volume;
        player.GetComponent<System_Destroy>().Invoke("Destroy", time);
        if (fade)
            player.GetComponent<System_Destroy>().StartCoroutine(player.GetComponent<System_Destroy>().FadeVolume());
    }

}
