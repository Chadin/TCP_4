﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectReferences : MonoBehaviour {

    [SerializeField] 
    GameObject obj_player;
    [SerializeField]
    Managers_GameOver manager_gameOver;
    [SerializeField]
    Managers_Score manager_score;
    [SerializeField]
    Managers_Tile manager_tile;

    #region GETS & SETS
    
    public GameObject Obj_Player
    {
        get { return this.obj_player; }
    }

    public Managers_GameOver Manager_GameOver
    {
        get { return this.manager_gameOver; }
    }

    public Managers_Score Manager_Score
    {
        get { return this.manager_score; }
    }

    public Managers_Tile Manager_Tile
    {
        get { return this.manager_tile; }
    }

    #endregion

}
