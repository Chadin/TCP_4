﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class System_SceneReseter : MonoBehaviour {

    [SerializeField]
    Managers_Tile tileManager;

	void Reset ()
    {
        SceneManager.LoadScene(0);
    }
	
    void NextStage()
    {
        tileManager.WinStage();
    }
}
