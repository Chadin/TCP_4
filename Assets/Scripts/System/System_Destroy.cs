﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class System_Destroy : MonoBehaviour {

    [SerializeField]
    GameObject playSwinPrefab;

	public void Destroy()
    {
        Destroy(this.gameObject);
    }

    public void DestroyPlayer()
    {
        GameObject player = GameObject.Find("Frog");
        GameObject.Instantiate(this.playSwinPrefab, player.transform.position, Quaternion.identity);
        GameObject.Destroy(player);
    }

    public IEnumerator FadeVolume()
    {
        yield return new WaitForSeconds(0.12f);
        this.GetComponent<AudioSource>().volume -= 0.05f;
        StartCoroutine(FadeVolume());
    }
}
