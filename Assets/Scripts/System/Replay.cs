﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Replay", menuName = "Replay")]
public class Replay : ScriptableObject
{
    public bool replaying, tutorial;

}
