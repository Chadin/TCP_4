﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : MonoBehaviour
{
    ObjectReferences objReferences;
    new Rigidbody2D rigidbody;
    LineRenderer tongue;
    bool lick;

    [SerializeField]
    float speed, minChangeTime, maxChangeTime, rotateSpeedAdditionMin, rotateSpeedAdditionMax, extraSpeed;
    float rotateSpeed;

	void Start ()
    {
        this.tongue = GameObject.Find("Tongue").GetComponent<LineRenderer>();
        this.rigidbody = GetComponent<Rigidbody2D>();
        this.objReferences = GameObject.FindObjectOfType<ObjectReferences>();
        StartCoroutine(ChangeDirectionRoutine());
        SetFirstAngle();
	}
	
	void Update ()
    {
        this.rigidbody.velocity = transform.up * this.speed * this.extraSpeed;
        this.transform.Rotate(new Vector3(0,0, this.rotateSpeed));
        if (this.extraSpeed == 1 && (this.transform.position.x > 3.33f || this.transform.position.x < -3.33f ||
           this.transform.position.y > 5.53f || this.transform.position.y < -5.53f))
            Destroy();
	}

    void OnMouseDown()
    {
        if(!this.objReferences.Manager_GameOver.GameOver && !this.lick)
        {
            StartCoroutine(Lick());
        }
    }

    IEnumerator Lick()
    {
        this.lick = true;
        for(int i = 0; i < 10; i++)
        {
            Vector3[] positions = new Vector3[2];
            positions[0] = this.tongue.transform.position;
            positions[1] = Vector2.Lerp(this.tongue.transform.position, this.transform.position, (float)i/10);
            if (this.tongue != null)
                this.tongue.SetPositions(positions);
            yield return new WaitForSeconds(0.01f);
        }
        GameObject.FindObjectOfType<Managers_Tile>().SetPowerup();
        Destroy();

    }

    public void SetFirstAngle()
    {
        int initialPos = Random.Range(0, 2);
        int direction = 1 - 2 * Random.Range(0, 2);
        switch (initialPos)
        {
            case 0:
                this.transform.position = new Vector2(3.33f * direction, Random.Range(-4.5f, 4.5f));
            break;
            case 1:
                this.transform.position = new Vector2(Random.Range(-2.57f, 2.57f), -5.53f * direction);
            break;
        }
        transform.up = Vector3.zero - transform.position;
    }

    IEnumerator ChangeDirectionRoutine()
    {
        yield return new WaitForSeconds(Random.Range(this.minChangeTime, this.maxChangeTime));
        this.extraSpeed = 1;
        int direction = 1 - 2 * Random.Range(0, 2);
        this.rotateSpeed = Mathf.Clamp(this.rotateSpeed + Random.Range(this.rotateSpeedAdditionMin, this.rotateSpeedAdditionMax) * direction
            , -2.5f, 2.5f);
        StartCoroutine(ChangeDirectionRoutine());
    }

    public void Destroy()
    {
        this.lick = false;
        Vector3[] positions = new Vector3[2];
        positions[0] = new Vector2(-3.2f, -3.2f);
        positions[1] = new Vector2(-3.2f, -3.2f);
        if(this.tongue != null)
            this.tongue.SetPositions(positions);
        Destroy(this.gameObject);
    }
}
