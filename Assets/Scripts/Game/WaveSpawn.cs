﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawn : MonoBehaviour
{
    [SerializeField]
    Managers_Tile tileManager;
    [SerializeField]
    GameObject wavePrefab;

	void Start ()
    {
        StartCoroutine(WaveSpawnRoutine());	
	}
	
	IEnumerator WaveSpawnRoutine()
    {
        yield return new WaitForSeconds(Random.Range(0.4f, 0.9f));
        float posX = Random.Range(-2.5f, 2.5f);
        float posY = Random.Range(-1f, 4.2f);
        this.tileManager.SceneryComponents.Add(GameObject.Instantiate(this.wavePrefab, new Vector2(posX, posY), Quaternion.identity));
        StartCoroutine(WaveSpawnRoutine());
    }
}
