﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effects_Circle : MonoBehaviour
{
    public int segments;
    public float radius;
    LineRenderer line;
    [SerializeField]
    Color color;

    void Start()
    {
        line = gameObject.GetComponent<LineRenderer>();

        line.SetVertexCount(segments + 1);
        line.useWorldSpace = false;
        CreatePoints();
        StartCoroutine(Increase());
    }

    IEnumerator Increase()
    {
        for(int i = 0; i < 40; i++)
        {
            yield return new WaitForSeconds(0.01f);
            this.color = new Color(this.color.r, this.color.g, this.color.b, this.color.a - 0.022f);
            this.line.startColor = this.line.endColor = this.color;
            this.radius += 0.025f;
            CreatePoints();
        }
        
    }

    void CreatePoints()
    {
        float x;
        float y;
        float z = 0f;

        float angle = 20f;

        for (int i = 0; i < (segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
            y = Mathf.Cos(Mathf.Deg2Rad * angle) * radius;

            line.SetPosition(i, new Vector3(x, y, z));

            angle += (360f / segments);
        }
    }
}
