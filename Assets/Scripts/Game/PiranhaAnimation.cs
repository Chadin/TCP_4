﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiranhaAnimation : MonoBehaviour
{
    [SerializeField]
    Sounds sounds;
    Managers_Tile tileManager;

    void Start()
    {
        this.sounds.Play(7, 1, 1, 1);
        this.tileManager = GameObject.FindObjectOfType<Managers_Tile>();
    }

	public void Eat()
    {
        this.sounds.Play(8, 1, 1, 1);
        this.tileManager.DestroyPlayer();
    }

    public void Sploosh()
    {
        this.sounds.Play(9, 1, 1, 1);
    }
}
