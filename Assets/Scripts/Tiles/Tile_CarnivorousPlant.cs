﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile_CarnivorousPlant : Tile_Global
{
    [SerializeField]
    Animator animator;

    new void Start () 
    {
        base.Start();
	}

    public override void Interaction()
    {
        base.Interaction();
        Invoke("EndGame", 0.5f);
    }

    void EndGame()
    {
        if(!GameObject.FindObjectOfType<Managers_GameOver>().GameOver)
        {
            this.animator.SetTrigger("Bite");
            if (this.objectReferences.Manager_Tile.CurrentTile == this.GetComponent<Tile_Global>())
            {
                this.objectReferences.Manager_Tile.GameStart = true;
                this.objectReferences.Manager_GameOver.SetGameOver(false, 1);
            }
        }
    }
}
