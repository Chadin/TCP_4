﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tile_Global : MonoBehaviour 
{
    protected ObjectReferences objectReferences;

    protected void Start()
    {
        this.objectReferences = GameObject.Find("ObjectReferences").GetComponent<ObjectReferences>();
    }

    public virtual void Interaction()
    {
        this.objectReferences.Manager_Tile.GameStart = true;
        this.objectReferences.Manager_Tile.CallRipple(this.transform);
    }
}
