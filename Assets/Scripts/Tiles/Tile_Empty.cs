﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile_Empty : Tile_Global
{
    new void Start () 
    {
        base.Start();
	}

    public override void Interaction()
    {
        this.objectReferences.Manager_GameOver.SetGameOver(false, 0);
    }
}
