﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile_Victory : Tile_Global
{
    new void Start()
    {
        base.Start();
    }

    public override void Interaction()
    {
        this.objectReferences.Manager_Tile.StartCoroutine(this.objectReferences.Manager_Tile.ShowGrade());
    }
}
