﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile_Shell : Tile_Global 
{
    bool submerged, stepped;
    [SerializeField]
    Animator animator;
    [SerializeField]
    float changeTime;

    new void Start()
    {
        base.Start();
        StartCoroutine(Routine());
    }

    public override void Interaction()
    {
        if(this.submerged)
        {
            this.objectReferences.Manager_GameOver.SetGameOver(false, 0);
            this.stepped = true;
            this.animator.SetTrigger("Miss");
        }
        else
        {
            base.Interaction();
            this.stepped = true;
            this.animator.SetTrigger("Stepped");
        }
    }

    IEnumerator Routine()
    {
        yield return new WaitForSeconds(this.changeTime);
        if (!this.stepped)
        {
            if (!submerged)
                this.animator.SetTrigger("Submerge");
            else
                this.animator.SetTrigger("Emerge");
            StartCoroutine(Routine());
        }
    }

    public void Change()
    {
        this.submerged = !this.submerged;
    }
}
